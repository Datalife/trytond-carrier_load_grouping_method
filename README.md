datalife_carrier_load_grouping_method
=====================================

The carrier_load_grouping_method module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-carrier_load_grouping_method/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-carrier_load_grouping_method)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
