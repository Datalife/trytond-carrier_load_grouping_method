# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import sale
from . import configuration
from . import load


def register():
    Pool.register(
        sale.SaleLine,
        configuration.Configuration,
        configuration.ConfigurationLoad,
        party.Party,
        load.LoadOrder,
        module='carrier_load_grouping_method', type_='model')
