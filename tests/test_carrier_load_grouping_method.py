# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class CarrierLoadGroupingMethodTestCase(ModuleTestCase):
    """Test Carrier Load Grouping Method module"""
    module = 'carrier_load_grouping_method'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            CarrierLoadGroupingMethodTestCase))
    # cannot implement tests here due to functionality is not complete
    # it neeeds other modules
    return suite
