# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class LoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    @classmethod
    def _group_line_key(cls, items, item):
        res = super()._group_line_key(items, item)
        key = cls._get_load_group_key(items, item)
        if key is not None:
            return res + (('load_group_key', str(key)), )
        return res

    @classmethod
    def _get_load_group_key(cls, items, item):
        return None
